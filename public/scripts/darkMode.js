const form = document.querySelector(".base-container");
const darkModeToggle = form.querySelector(".switch-input");

let darkMode = localStorage.getItem("darkMode");

const  enableDarkMode = () => {
    document.body.classList.add("dark-mode");
    localStorage.setItem("darkMode", "enabled");
}

const  disableDarkMode = () => {
    document.body.classList.remove("dark-mode");
    localStorage.setItem("darkMode", null);
}

if(darkMode === 'enabled'){
    enableDarkMode();
    darkModeToggle.checked = true;
}

darkModeToggle.addEventListener("click", () => {
    darkMode = localStorage.getItem('darkMode');
    if(darkMode !== 'enabled') {
        enableDarkMode();
    } else {
        disableDarkMode();
    }
})
