const search = document.querySelector('input[placeholder="search pupil"]');
const pupilsContainer = document.querySelector('.pupils');

search.addEventListener("keyup", function (event) {
    if (event.key === "Enter") {
        event.preventDefault();
        const data = {search: this.value};

        fetch("/search", {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }).then(function (response) {
            return response.json();
        }).then(function (pupils) {
            pupilsContainer.innerHTML = "";
            loadPupils(pupils)
        });
    }
});

function loadPupils(pupils) {
    pupils.forEach( pupil => {
            console.log(pupil);
            createPupil(pupil);
        }
    );
}

function createPupil(pupil) {
    const template = document.querySelector('#pupil-template');
    const clone = template.content.cloneNode(true);

    const name = clone.querySelector("p");
    name.innerHTML = pupil.name;

    pupilsContainer.appendChild(clone);
}