
const exerciseContainer = document.querySelector('.exercise-list');
const add = exerciseContainer.querySelector('.add');
const remove = exerciseContainer.querySelector('.remove');
const li = exerciseContainer.querySelector('.add-exercise');
const exerciseName = li.querySelector('.exercise-name');
const exerciseWeight = li.querySelector('.exercise-weight');
const exerciseSeries = exerciseContainer.querySelector('.series');
const exerciseReps = exerciseContainer.querySelector('.reps');



add.addEventListener("click", function (event) {

    if(exerciseName.value.length !== 0
        || exerciseWeight.value.length !== 0
        || exerciseReps.value.length !== 0
        || exerciseSeries.value.length !== 0  ){
        event.preventDefault();
        const data = {name: exerciseName.value, weight: exerciseWeight.value ,series: exerciseSeries.value,reps: exerciseReps.value};
        fetch("/add", {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then(function (response) {
            return response.json();
        }).then(function (exercises) {
            exerciseContainer.innerHTML = "";
            loadExercises(exercises)
        });
    }
});

function loadExercises(exercises) {
    exercises.forEach(exercise => {
        createExercise(exercise);
    });
}

function createExercise(exercise) {
    const template = document.querySelector('#add-exercise-template');
    const clone = template.content.cloneNode(true);

    const li = clone.querySelector(".li");
    li.id = exercise.id;

    const exerciseName = clone.querySelector(".exercise-name");
    exerciseName.innerHTML =exercise.name;

    const exerciseWeight = clone.querySelector(".exercise-weight");
    exerciseWeight.innerHTML =exercise.weight;

    const exerciseReps = clone.querySelector(".exercise-weight");
    exerciseReps.innerHTML =exercise.reps;

    const exerciseSeries = clone.querySelector(".exercise-weight");
    exerciseSeries.innerHTML =exercise.series;

    exerciseContainer.appendChild(clone);
}


remove.addEventListener("click", function (event) {


});