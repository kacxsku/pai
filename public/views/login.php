<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>GymKing</title>
    <script src="https://kit.fontawesome.com/64ec48345e.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="public/css/buttons.css">
    <link rel="stylesheet" href="public/css/responsible-style.css">
</head>

<body class="main-body">
            
        <img class="login-logo" src="public/assets/logo.svg" width="280">

        <button class="register"><a href="/registration">Register</a> </button>

        <div class="login-container ">
            <div class="circle ">
                <img class="main-page-photo " src="public/assets/mainPagePhoto.gif " width="333" height="490">
            </div>
            <form class="login-form" action="login" method="POST">
                <div class="messages">
                    <?php
                    if(isset($messages)){
                        foreach($messages as $message) {
                            echo $message;
                        }
                    }
                    ?>
                </div>
                <input name="email" type="text" placeholder="email@email.com">
                <input name="password" type="password" placeholder="Password">
                <button class="login-button" type="submit">Log In</button>
            </form>
        </div>
        <div class="placeholder"></div>

        <section class="features-section ">
            <ul class="features-list ">
                <li>
                    <a>Training Planner</a>
                    <blockquote> With our app You can plan your trainings.
                    </blockquote>
                </li>
                <li>
                    <a>Training Plan Library</a>
                    <blockquote>Our Training Plan software can be used to 
                        develop fitness, endurance, technique, and strength
                         based training sessions for your business and those
                          training sessions can then be used to create a library
                           of training plans. </blockquote>
                </li>
                <li>
                    <a>Training Schedule</a>
                    <blockquote>With Our app you can plan your training</blockquote>
                </li>
                <li>
                    <a>Stats</a>
                    <blockquote>With Our app you can chech your progress. </blockquote>
                </li>
                <li>
                    <a>For Trainers and Pupils</a>
                    <blockquote>Our app is made for trainers and theirs pupils </blockquote>
                </li>
            </ul>
        </section>

        <div class="placeholder"></div>

        <div class="description-section ">
            <b>Do fitness <span> with us, achieving high results.</b>
                <div class="page-description">
                    <img class="stats-page" src="public/assets/stats.png" width="200" height="200">
                    <blockquote>Our Training Plan software can be used to 
                        develop fitness, endurance, technique, and strength
                         based training sessions for your business and those
                          training sessions can then be used to create a library
                           of training plansOur Training Plan software can be used to 
                           develop fitness, endurance, technique, and strength
                            based training sessions for your business and those
                             training sessions can then be used to create a library
                              of training plans
                    </blockquote>
                </div>
        </div>  
</body>

</html>