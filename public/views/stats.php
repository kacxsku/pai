<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Stats</title>
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="public/css/buttons.css">
    <link rel="stylesheet" href="public/css/responsible-style.css">

    <script src="https://kit.fontawesome.com/64ec48345e.js" crossorigin="anonymous"></script>
    <script type="text/javascript" src="./public/scripts/darkMode.js" defer></script>
    <script type="text/javascript" src="./public/scripts/jquery.min.js" defer ></script>
    <script type="text/javascript" src="./public/scripts/Chart.min.js" defer></script>
    <script type="text/javascript" src="./public/scripts/ line-db-php.js" defer></script>

    <script src="https://www.gstatic.com/charts/loader.js"></script>

</head>

<body>
    <div class="base-container">
        <?php include('menu.php')?>

        <main>
            <?php
            $headerName = 'Stats';
            include('header.php')
            ?>

            <form class="calendar-form" action="stats" method="POST">
                <div class="scroll-list">
                    <select class="drop-down-list" name="exercise-option">
                        <?php foreach ($exercises as $exercise): ?>
                            <option><?=$exercise?></option>
                        <?php endforeach; ?>
                    </select>
                    <button name="get-results-button" class="submit">Create Chart</button>
                </div>
            </form>


            <div id="myChart" style="max-width:700px; height:400px"></div>


<!--            <button  class="submit" type="button"> Add exercise goal</button>-->
        </main>

    </body>

</html>