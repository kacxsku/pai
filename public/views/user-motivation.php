<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Contact</title>
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="public/css/buttons.css">
    <link rel="stylesheet" href="public/css/responsible-style.css">  
    <script src="https://kit.fontawesome.com/64ec48345e.js" crossorigin="anonymous"></script>
    <script  type="text/javascript" src="./public/scripts/darkMode.js" defer></script>

</head>
<body>
<div class="base-container">
    <?php include('menu.php')?>
    <main>
        <?php
        $headerName = 'Add Your Motivation';
        include('header.php')
        ?>

        <form class="post-form" action="addMotivation" method="POST">
            <p>Add your motivaton to training!</p>
            <div class="messages">
                <?php
                if(isset($messages)){
                    foreach($messages as $message) {
                        echo $message;
                    }
                }
                ?>
            </div>
                    <label>
                        <textarea name="motivation"></textarea>
                    </label>
            <button name="set-motivation-button" class="login-button" type="submit">Save</button>
        </form>
    </main>
</div>
</body>
</html>