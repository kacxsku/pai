<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Contact</title>

  <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="public/css/buttons.css">
    <link rel="stylesheet" href="public/css/responsible-style.css">
    <script src="https://kit.fontawesome.com/64ec48345e.js" crossorigin="anonymous"></script>
    <script  type="text/javascript" src="./public/scripts/darkMode.js" defer></script>


</head>
<body>
<div class="base-container">

    <?php include('menu.php')?>

    <main>
        <?php
        $headerName = 'Send us a message';
        include('header.php')
        ?>
  <hr>

      <form class="post-form" action="contact" method="POST">
          <div class="messages">
              <?php
              if(isset($messages)){
                  foreach($messages as $message) {
                      echo $message;
                  }
              }
              ?>
          </div>
        <p>give a feedback, to better develop our app!!!</p>

        <textarea  name="email-message"></textarea>
        <button name='send-email-button' class="login-button" type="submit">Send Email</button>
      </form>

  </main>


</div>
</body>
</html>