
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="public/css/menu.css">
    <link rel="stylesheet" href="public/css/responsible-menu.css">
    <script src="https://kit.fontawesome.com/64ec48345e.js" crossorigin="anonymous"></script>

</head>
<body>
    
<nav class="navbar">
    <ul class="navbar-nav">

        <li class="nav-item">
            <a class="logo" >
                 <i class="fas fa-arrow-circle-right"></i>
                <img class="logo-img" src="public/assets/logo.svg" width="250">
            </a>
        </li>

        <li class="nav-item">
            <a class="user-photo" href="/account" >
                    <img class="avatar-menu" alt="Avatar" src="<?= $user->getUserPhoto(); ?>"  width="200" height="200">
                    <p class="username"><?= $user->getName(); ?></p>
            </a>
        </li>

        <li class="plc-holder">

        </li>

        
        <li class="nav-item">
            <a class="nav-link" href="/account" >
            <i class="fas fa-user-alt"></i>
            <span class="link-text logo-text">
                    Account
                </span>
            </a>
        </li>


        <li class="nav-item">
            <a class="nav-link" href="/schedule">
                <i class="far fa-clock"></i>
                <span class="link-text logo-text">
                    Training Schedule
                </span>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="/diary">
                <i class="far fa-calendar-alt"></i>
                <span class="link-text logo-text">
                    Training Diary
                </span>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="/stats">
                <i class="far fa-chart-bar"></i>
                <span class="link-text logo-text">
                    Stats
                </span>
            </a>
        </li>



        <li class="nav-item">
            <a class="nav-link" href="/settings">
                <i class="fas fa-cog"></i>
                <span class="link-text logo-text"> Settings </span>
            </a>
        </li>

        <li class="nav-item">
            <form class="logout-form" action="logout" method="POST">
                <button class="logout-button" type="submit">
                    <i class="fas fa-sign-out-alt"></i>
                    <span>Logout</span>
                    </button>
            </form>
        </li>
</ul>
</nav>

</body>
</html>
