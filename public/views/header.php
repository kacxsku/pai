<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <script  type="text/javascript" src="./public/scripts/darkMode.js" defer></script>
</head>
<body>
<header>
    <div class="header-name">
        <b><?=$headerName?></b>
        <img class="logo-header-img" src="public/assets/logo.svg" width="250">
    </div>
    <div class="horizontal-line"></div>
</header>
<hr>
</body>
</html>