<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Training Diary</title>
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="public/css/buttons.css">
    <link rel="stylesheet" href="public/css/responsible-style.css">

    <script src="https://kit.fontawesome.com/64ec48345e.js" crossorigin="anonymous"></script>
    <script  type="text/javascript" src="./public/scripts/darkMode.js" defer></script>
    <script  type="text/javascript" src="./public/scripts/addNewLiElement.js" defer></script>

</head>

<body>
    <div class="base-container">

        <?php include('menu.php')?>

        <main>
            <?php
            $headerName = 'Training Diary';
            include('header.php')
            ?>

            <div class="dairy-div">
                <form class="calendar-form" action="diary" method="POST">
                    <input type="date" name="calendar" onload="getDate()">
                    <button name="get-exercises-button" class="submit">Check Dairy</button>
                </form>

                    <section class="exercise-info">
                        <ul class="exercise-list">

                            <?php foreach($exercises as $exercise): ?>
                                <li>
                                    <div class="exercise-info">
                                        <input type="checkbox" value="exercise1">
                                        <label for="exercise1"><?= $exercise->getName(); ?>></label>
                                        <i  class="fas fa-ellipsis-h"></i>
                                    </div>
                                    <div class="exercise-details">
                                        <input type="number" min="1" max="100">
                                        <input type="number" min="1" max="100">
                                    </div>
                                </li>

                            <?php endforeach; ?>

                            <li class="add-exercise">
                                <div class="exercise-info">
                                    <a href="#" class="add">
                                        <i class="fas fa-plus-circle" ></i>
                                    </a>
                                    <input type="text" class="exercise-name">
                                    <input type="text" class="exercise-weight">
                                </div>
                                <div class="exercise-details">
                                    <input type="number" class="series" min="1" max="100">
                                    <input type="number" class="reps" min="1" max="100">
                                </div>
                            </li>
                        </ul>
                    </section>

            </div>
        </main>
    </div>
</body>


<template class="add-exercise-template">
    <li class="exercise-info" id="">
        <div class="exercise-info">
            <a href="#" class="remove">
                <i class="fas fa-minus-circle"></i>
            </a>
            <label class="exercise-name">name</label>
            <label class="exercise-weight">weight</label>
            <label class="exercise-series">series</label>
            <label class="exercise-weight">weight</label>
        </div>

    </li>
</template>

</html>


