<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>add pupil</title>
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="public/css/buttons.css">
    <link rel="stylesheet" href="public/css/responsible-style.css">

    <script src="https://kit.fontawesome.com/64ec48345e.js" crossorigin="anonymous"></script>
    <script  type="text/javascript" src="./public/scripts/darkMode.js" defer></script>

</head>
<body>
    <div class="base-container">

        <?php include('menu.php')?>



        <main>
            <?php
            $headerName = 'Add Pupil';
            include('header.php')
            ?>

            <form class="addPupil" action="addPupil" method="POST">
                <div class="list-div">
                    <h3>provide the pupils's e-mail address</h3>
                    <div class="messages">
                        <?php
                        if(isset($messages)){
                            foreach($messages as $message) {
                                echo $message;
                            }
                        }
                        ?>
                    </div>
                    <input  name="userName" class="add-input" type="text" >
                </div>
                <button name="add-pupil-button" class="login-button" type="submit">Add</button>
            </form>


        </main>
</body>
</html>