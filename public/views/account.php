<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Your Account</title>
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="public/css/buttons.css">
    <link rel="stylesheet" href="public/css/responsible-style.css">
    
    <script src="https://kit.fontawesome.com/64ec48345e.js" crossorigin="anonymous"></script>
    <script  type="text/javascript" src="./public/scripts/darkMode.js" defer></script>

</head>

<body>
    <div class="base-container">
        <?php include('menu.php')?>
          <main>
              <?php
              $headerName = 'Your Account';
              include('header.php')
              ?>

            <div class="main-container">
                <div class="management-container">
                    <div class="user-info">
                        <div class="avatar-name">
                            <img class="avatar" alt="Avatar" src="<?= $user->getUserPhoto(); ?>" >
                            <p class="username"><?= $user->getName(); ?></p>
                        </div>

                        <?php if (strcmp($_COOKIE['role'], "USER") == 0)
                            echo "<div class='user-motivation'>
                                <blockquote>{$user->getUserMotivation()}</blockquote>
                            </div>"
                        ?>
                    </div>

                    <?php if (strcmp($_COOKIE['role'], "USER") == 0)
                        echo "<div class='action'>
                        <div class='add-user-motivation'>
                            <i class='fas fa-plus'></i>
                            <a href='/addMotivation'>add-motivation</a>
                        </div>
                    </div>"
                        ?>

                    <?php if (strcmp($_COOKIE['role'], "TRAINER") == 0)
                        echo "<div class='user-buttons'>
                        <form action='addPlan' method='POST'>
                            <button class='submit' >Add your own plan</button>
                        </form>
                        <form action='addPupil' method='POST'>
                            <button class='submit'>Add pupil</button>
                        </form>
                        <form action='myPupils' method='POST'>
                            <button class='submit'>My pupils</button>
                        </form>
                    </div>"
                        ?>

                </div>

                <div class="notifications-container">
                    <div class="notifications">
                        <i class="far fa-bell"></i>
                        <h3>Notifications</h3>
                    </div>

                    <ul class="notification-list">
                        <?php foreach($notifications as $notification): ?>
                        <li>
                            <div class="notification">
                                <p class="notification-title"><?= $notification->getTitle(); ?></p>
                                <p class="notification-text"><?= $notification->getBody(); ?></p>
                            </div>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
            </main>
    </div>
    
</body>

</html>