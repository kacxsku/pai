<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pupils</title>
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="public/css/buttons.css">
    <link rel="stylesheet" href="public/css/responsible-style.css">

    <script src="https://kit.fontawesome.com/64ec48345e.js" crossorigin="anonymous"></script>
    <script  type="text/javascript" src="./public/scripts/darkMode.js" defer></script>
    <script type="text/javascript" src="./public/scripts/search.js" defer></script>

</head>
<body>
    <div class="base-container">
        <?php include('menu.php')?>

        <main>
            <?php
            $headerName = 'My pupils';
            include('header.php')
            ?>

            <div class="search-bar">
                <input placeholder="search pupil">
            </div>

            <section class="pupils">
                    <ul class="pupils-list">
                        <?php foreach($pupils as $pupil): ?>
                            <li class="pupil-tmpl-style">
                                <p><?= $pupil->getUser()->getName(); ?></p>
                                <i class="fas fa-user"></i>
                            </li>
                        <?php endforeach; ?>

                    </ul>
            </section>
        </main>
</body>


<template id="pupil-template">
    <ul class="pupils-list">
        <li id="" class="pupil-tmpl-style">
            <p>title</p>
            <i class="fas fa-user"></i>
        </li>
    </ul>

</template>

</html>