<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>add plan</title>

    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="public/css/buttons.css">
    <link rel="stylesheet" href="public/css/responsible-style.css">

    <script src="https://kit.fontawesome.com/64ec48345e.js" crossorigin="anonymous"></script>
    <script  type="text/javascript" src="./public/scripts/addNewLiElement.js" defer></script>
    <script  type="text/javascript" src="./public/scripts/darkMode.js" defer></script>


</head>
<body>
    <div class="base-container">
        <?php include('menu.php')?>
        <main>
            <header>
                <div class="header-name">
                    <b>Add Plan</b>
                </div>
                <div class="horizontal-line"></div>
                <form class="logout-form" action="logout" method="POST">
                    <button class="login-button" type="submit">Log Out</button>
                </form>
            </header>
           <hr>

            <form class="plan" action="addPlan" method="POST">
                <div class="list-div">
                    <input  class="add-training-input" type="text" placeholder="Training Title">

                    <ul class="list-plan">

                        <li class="new-plan">

                            <div class="exercise-info">
                                <a  class="add" >
                                    <i class="fas fa-plus-circle"></i>
                                </a>

                                <div class="exercise-management">
                                        <input  class="add-exercise-input" type="text" placeholder="add exercise">
                                        <input  class="add-exercise-input" type="text" placeholder="add description">
                                </div>
                            </div>
                        </li>

                    </ul>

                </div>
                <a class="submit" href="#">Add plan</a>
            </form>


        </main>


</body>
</html>