<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>My plans</title>

    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="public/css/buttons.css">
    <link rel="stylesheet" href="public/css/responsible-style.css">

    <script src="https://kit.fontawesome.com/64ec48345e.js" crossorigin="anonymous"></script>
    <script  type="text/javascript" src="./public/scripts/darkMode.js" defer></script>

</head>
<body>
    <div class="base-container">

        <?php include('menu.php')?>


        <main>
            <?php
            $headerName = 'My Training Plans';
            include('header.php')
            ?>

           <div class="list-div">
            <ul class="list">
                <li>
                    gww
                    <i class="fas fa-ellipsis-h"></i>            
                </ul>
           </div>

        </main>



</body>
</html>