<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>FAQs</title>
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="public/css/buttons.css">
    <link rel="stylesheet" href="public/css/responsible-style.css">
    <script src="https://kit.fontawesome.com/64ec48345e.js" crossorigin="anonymous"></script>
    <script  type="text/javascript" src="./public/scripts/darkMode.js" defer></script>

</head>
<div class="base-container">

    <?php include('menu.php')?>

    <main>
        <?php
            $headerName = 'FAQs';
            include('header.php')
            ?>


    </main>
</div>
</main>



</div>
</body>
</html>