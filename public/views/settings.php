<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Settings</title>
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="public/css/buttons.css">
    <link rel="stylesheet" href="public/css/responsible-style.css">


    <script src="https://kit.fontawesome.com/64ec48345e.js" crossorigin="anonymous"></script>
    <script  type="text/javascript" src="./public/scripts/darkMode.js" defer></script>
    <script  type="text/javascript" src="./public/scripts/Copy.js" ></script>

</head>

<body>
    <div class="base-container">
        <?php include('menu.php')?>

        <main>
            <?php
            $headerName = 'Settings';
            include('header.php')
            ?>

            <ul class="setting-list">
                <li>
                    <a class="row-style" href="/uploadImage">
                        <div class="setting-name">
                            <i class="fas fa-camera"></i>
                            Change profil photo
                        </div>
                    </a>
                </li>

                <li>
                    <a class="row-style" >
                        <div class="setting-name">
                            <i class="fas fa-moon"></i>
                            Dark Mode
                        </div>
                        <label class="toggle-switch">
                            <input class="switch-input"  id="theme-switch" type="checkbox" />
                            <span class="switch-label" ></span> 
                            <span class="switch-handle"></span> 
                        </label>
                    </a>
                </li>

                <li>
                    <a class="row-style">
                        <div class="setting-name">
                            <i class="fas fa-globe"></i>
                            Language
                        </div>
                        <button class="language">
                            en
                        </button>
                    </a>
                </li>
    
                <li>
                    <a class="row-style" href="/contact">
                        <div class="setting-name">
                            <i class="fas fa-users"></i>
                            Contact
                        </div>
                    </a>
                </li>
    
                <li>
                    <a class="row-style" href="/faqs">
                        <div class="setting-name">
                            <i class="fas fa-question-circle"></i>
                            FAQ
                        </div>
                    </a>
                </li>

                <li>
                    <a class="row-style" id="shareLink" href="#" onclick="copyToClipboard()">
                        <div class="setting-name">
                            <i class="far fa-share-square"></i>
                            Invite Friends
                        </div>
                    </a>
                </li>


            </ul>
<!--            <a class="submit" href="account.php" >Apply changes</a>-->
        </main>
    </div>


</body>

</html>