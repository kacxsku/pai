<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registration</title>
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="public/css/buttons.css">
    <link rel="stylesheet" href="public/css/responsible-style.css">
    <script src="https://kit.fontawesome.com/64ec48345e.js" crossorigin="anonymous"></script>

    <script  type="text/javascript" src="./public/scripts/script.js" defer></script>

</head>

<body class="main-body">

    <div class="registration-header">
        <h1>Registration</h1>
    </div>

    <a class="registration-back-button" href="/">
        <i class="fas fa-long-arrow-alt-left"></i>
    </a>

    <div class="registration">
            <img src="public/assets/logo.svg">
        <div class="registration-container">

            <form class="registration-form" action="registration" method="POST">
                <div class="messages">
                    <?php
                    if(isset($messages)){
                        foreach($messages as $message) {
                            echo $message;
                        }
                    }
                    ?>
                </div>
                <input name="email" type="text" placeholder="Email">
                <input name="password" type="password" placeholder="password">
                <input name="confirmedPassword" type="password" placeholder="Confirm password">
                <input name="name" type="text" placeholder="Name">
                <input name="age" type="number" placeholder="Age">
                <button class="login-button" type="submit">Register</button>
            </form>
<!--            <a class="registration-back-button" href="/">-->
<!--            <i class="fas fa-long-arrow-alt-left"></i>-->
<!--            </a>-->
        </div>

    </div>


</body>

</html>