<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exercise</title>
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="public/css/buttons.css">
    <link rel="stylesheet" href="public/css/responsible-style.css">

    <script src="https://kit.fontawesome.com/64ec48345e.js" crossorigin="anonymous"></script>
    <script  type="text/javascript" src="./public/scripts/darkMode.js" defer></script>

</head>
<body>
    <div class="base-container">

        <?php include('menu.php')?>

        <main>
            <?php
            $headerName = 'Training Diary';
            include('header.php')
            ?>
           <hr>

           <div class="exercise-description">

               
            <h3><?=$exercise->getName()?></h3>
            <hr>
            <p><?=$exercise->getDescription()?></p>

           </div>

        </main>



</body>
</html>