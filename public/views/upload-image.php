<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Upload photo</title>

  <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="public/css/buttons.css">
    <link rel="stylesheet" href="public/css/responsible-style.css">
  <script src="https://kit.fontawesome.com/64ec48345e.js" crossorigin="anonymous"></script>
    <script  type="text/javascript" src="./public/scripts/reload.js" defer></script>
    <script  type="text/javascript" src="./public/scripts/darkMode.js" defer></script>

</head>
<body>
<div class="base-container">
    <?php include('menu.php')?>
    <main>
        <?php
        $headerName = 'Change Profile Photo';
        include('header.php')
        ?>
      <form class="upload-photo" action="uploadImage" method="POST" ENCTYPE="multipart/form-data">
          <div class="messages">
              <?php
              if(isset($messages)){
                  foreach($messages as $message) {

                      echo $message;
                  }
              }
              ?>
          </div>
          <img id="blah" src="#" alt="Your Profile Photo"/>
          <input name="file" type='file' id="imgInp" />
          <button name="set-image-button" class="login-button" type="submit" >Save</button>
      </form>
  </main>


<script>
  imgInp.onchange = evt => {
  const [file] = imgInp.files
  if (file) {
    blah.src = URL.createObjectURL(file)
  }
}

</script>

</div>
</body>
</html>