<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Training Schedule</title>
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="public/css/buttons.css">
    <link rel="stylesheet" href="public/css/responsible-style.css">

    <script src="https://kit.fontawesome.com/64ec48345e.js" crossorigin="anonymous"></script>
    <script  type="text/javascript" src="./public/scripts/darkMode.js" defer></script>

</head>

<body>
    <div class="base-container">
        <?php include('menu.php')?>

        <main>
            <?php
            $headerName = 'Training Schedule';
            include('header.php')
            ?>

              <div class="training-title">
                  <b><?=$training->getName()?></b>
                  <hr>
              </div>

              <ul class="training-list">
                  <?php foreach($exercises as $exercise): ?>
                <li>
                      <label for="exercise1"><?=$exercise->getName()?></label>
                      <div class="add-exercise">
                        <input type="checkbox" value="exercise" name="exercise-checkbox" checked>
                          <a>
                              <i class="fas fa-ellipsis-h"></i>
                          </a>
                      </div>
                </li>
                  <?php endforeach; ?>
            </ul>
          </main>
      </div>

</body>

</html>