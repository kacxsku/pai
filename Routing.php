<?php

require_once 'source/controllers/DefaultController.php';
require_once 'source/controllers/SecurityController.php';
require_once 'source/controllers/UserController.php';
require_once 'source/controllers/TrainerController.php';
require_once 'source/controllers/DiaryController.php';
require_once 'source/controllers/StatsController.php';
require_once 'source/controllers/ScheduleController.php';


class Routing{

    public static $routes;

    public static function get($url, $view) {
        self::$routes[$url] = $view;
    }

    public static function post($url, $view) {
        self::$routes[$url] = $view;
    }

    public  static function run($url) {
        $action = explode("/",$url)[0];
        if(!array_key_exists($action, self::$routes)) {
            die("Wrong url!");
        }

        if(isset($_COOKIE['user'])) {
            if ($url == 'login' || $url == 'registration' || $url =='' ) {
                $controller = self::$routes[explode("/", "account")[0]];
                $object = new $controller;
                $action = 'account';
            }
            else{
                $controller = self::$routes[explode("/",$url)[0]];
                $object = new $controller;
                $action = $action ?: 'index';
            }
        }
        elseif ($url == 'registration'){
            $controller = self::$routes[$action];
            $object = new $controller;
            $action = 'registration';
        }
        else {
            $controller = self::$routes[explode("/","login")[0]];
            $object = new $controller;
            $action = 'login';
        }


        $object->$action();
    }
}