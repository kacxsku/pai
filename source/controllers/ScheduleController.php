<?php

require_once 'AppController.php';
require_once __DIR__.'/../models/User.php';
require_once __DIR__.'/../repository/UserRepository.php';
require_once __DIR__.'/../repository/NotificationRepository.php';
require_once __DIR__.'/../repository/TrainingRepository.php';
require_once __DIR__.'/../repository/TrainingDetailsRepository.php';
require_once __DIR__.'/../repository/ExerciseRepository.php';


class ScheduleController extends AppController
{

    private User $user;
    private UserRepository $userRepository;
    private NotificationRepository $notificationRepository;
    private TrainingRepository  $trainingRepository;
    private TrainingDetailsRepository  $trainingDetailsRepository;
    private ExerciseRepository $exerciseRepository;

    public function __construct(){
        parent::__construct();
        $this->userRepository = new UserRepository();
        $this->notificationRepository = new NotificationRepository();
        $this->user = $this->userRepository->getUser($_COOKIE['user']);
        $this->trainingRepository = new TrainingRepository();
        $this->trainingDetailsRepository = new TrainingDetailsRepository();
        $this->exerciseRepository = new ExerciseRepository();
    }

}