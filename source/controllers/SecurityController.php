<?php

require_once 'AppController.php';
require_once __DIR__.'/../repository/UserRepository.php';

class SecurityController extends AppController {

    private $cookieName;
    private UserRepository $userRepository;

    public function __construct(){
        parent::__construct();
        $this->userRepository = new UserRepository();
        $this->cookieName = 'user';
    }

    public function login(){
        if (!$this->isPost()) {
            return $this->render('login');
        }
        $email = $_POST['email'];
        $password = $_POST['password'];
        $user = $this->userRepository->getUser($email);

        if (!$user) {
            return $this->render('login', ['messages' => ['User not exist!']]);
        }
        if ($user->getLogin() !== $email) {
            return $this->render('login', ['messages' => ['User with this email not exist!']]);
        }
        if (!password_verify($password,$user->getPassword() )) {
            return $this->render('login', ['messages' => ['Wrong password!']]);
        }

        if(!isset($_COOKIE[$this->cookieName])){
            $cookieLoginValue = $user->getLogin();
            $cookiePhotoValue = $user->getUserPhoto();
            $cookieRoleValue = $user->getRole();

            setcookie($this->cookieName,$cookieLoginValue,time() + (86400 * 30),"/");
            setcookie('photo',$cookiePhotoValue,time() + (86400 * 30),"/");
            setcookie('role',$cookieRoleValue,time() + (86400 * 30),"/");
        }

        $url = "http://$_SERVER[HTTP_HOST]";
        header("Location: {$url}/account");
    }


    public function registration(){
        if(!$this->isPost()){
            return $this->render('registration');
        }

        $user = new User(
            null,
            $_POST['email'],
            password_hash($_POST['password'],PASSWORD_DEFAULT),
            $_POST['name'],
            "",
            $_POST['age'],
            "",
            null
        );

        $message = $this->userRepository->saveUser($user);

        return $this->render('registration', ['messages' => [$message]]);
    }

    public function logout(){
        if (isset($_COOKIE[$this->cookieName])) {
            setcookie($this->cookieName, '', time() - (86400 * 30), "/");
            setcookie("role", '', time() - (86400 * 30), "/");
            setcookie("photo", '', time() - (86400 * 30), "/");
            $url = "http://$_SERVER[HTTP_HOST]";
            header("Location: {$url}/");
        }
    }

}