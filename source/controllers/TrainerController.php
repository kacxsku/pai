<?php

require_once 'AppController.php';
require_once __DIR__.'/../models/User.php';
require_once __DIR__.'/../repository/UserRepository.php';
require_once __DIR__.'/../repository/TrainerRepository.php';
require_once __DIR__.'/../repository/ExerciseRepository.php';


class TrainerController extends AppController
{
    private UserRepository $userRepository;

    private TrainerRepository $trainerRepository;
    private array $messages = [];
    private User $user;

    public function __construct(){
        parent::__construct();
        $this->userRepository = new UserRepository();
        $this->user = $this->userRepository->getUser($_COOKIE['user']);
        $this->trainerRepository = new TrainerRepository();
    }


    public function addPlan(){
//        if($this->isPost()
//            && isset($_POST['add-pupil-button'])
//            && $this->userRepository->getUser()
//        ){
//            $userName = $_POST['userName'];
//            if($this->userRepository->getUser($userName) == null){
//                $this->messages [] = "user Not Exist";
//            }
//            else{
//                //add relation to db
////                $this->messages [] = "user Not Exist";
//
//            }
//            //user Exist???
//
//
//        }
        $this->render('add-plan', ['messages' => $this->messages,'user' => $this->user]);
    }

    public function addPupil(){
        if($this->isPost()
            && isset($_POST['add-pupil-button'])){
            $userName = $_POST['userName'];
            $userToSave = $this->userRepository->getUser($userName);
            if( $userToSave== null){
                $this->messages [] = "user Not Exist";
            }
            else{
                $this->messages [] = $this->trainerRepository->addPupil($userToSave);
            }
        }
        $this->render('add-pupil',['messages' => $this->messages,'user' => $this->user]);
    }

    public function myPupils(){
        $pupils= $this->trainerRepository->getAllPupils();
        $this->render('my-pupils', ['pupils' => $pupils,'messages' => $this->messages,'user' => $this->user]);
    }


    public function search() {
        $contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';
        if ($contentType === "application/json") {
            $content = trim(file_get_contents("php://input"));
            $decoded = json_decode($content, true);
            header('Content-type: application/json');
            http_response_code(200);
            $searchValue =$this->trainerRepository->getPupilByName($decoded['search']);
            echo json_encode($searchValue);
        }
    }
}