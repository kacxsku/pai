<?php

require_once  __DIR__."/../repository/UserRepository.php";
require_once  __DIR__."/../repository/ExerciseRepository.php";

class StatsController extends AppController
{

    private UserRepository $userRepository;
    private User $user;
    private ExerciseRepository $exerciseRepository;

    public function __construct(){
        parent::__construct();
        $this->userRepository = new UserRepository();
        $this->user = $this->userRepository->getUser($_COOKIE['user']);
        $this->exerciseRepository = new ExerciseRepository();
    }

    public function stats(){
        $exercises = $this->exerciseRepository->getAllUsersExercises($this->user->getId());
        if($this->isPost()
            && isset($_POST['get-results-button'])) {
            $exerciseName = $_POST['get-results-button'];
            $results = $this->exerciseRepository->getResultsByExerciseName($exerciseName, $this->user);
        }
        $this->render('stats', ['user' => $this->user, 'exercises' => $exercises, 'results'=>  $results]);
    }


}