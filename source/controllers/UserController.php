<?php

require_once 'AppController.php';
require_once  __DIR__."/../repository/UserRepository.php";

class UserController extends AppController
{
    const MAX_FILE_SIZE = 1024*1024;
    const SUPPORTED_TYPES = ['image/png', 'image/jpeg'];
    const UPLOAD_DIRECTORY = '/../public/uploads/';
    private array $messages = [];
    const MAX_CHAR = 100;

    private UserRepository $userRepository;
    private User $user;

    public function __construct(){
        parent::__construct();
        $this->userRepository = new UserRepository();
        $this->user = $this->userRepository->getUser($_COOKIE['user']);
    }

    public function uploadImage(){
        if($this->isPost()
            && isset($_POST['set-image-button'])
            && is_uploaded_file($_FILES['file']['tmp_name'])
            && $this->validate($_FILES['file'])){
            if($_FILES['file']['size'] > self::MAX_FILE_SIZE) {
                $this->messages[] = 'File is too large for destination file system.';
            }
            if(!isset($file['type']) && !in_array($_FILES['file']['type'], self::SUPPORTED_TYPES)) {
                $this->messages[] = 'File type is not supported.';
            }
            else{
                move_uploaded_file(
                    $_FILES['file']['tmp_name'],
                    dirname(__DIR__).self::UPLOAD_DIRECTORY.$_FILES['file']['name']
                );
                $this->user = $this->userRepository->getUser($_COOKIE['user']);
            }
            $this->messages[] = $this->userRepository->updatePhoto($_COOKIE['user'],$_FILES['file']['name']);
        }
        $this->user = $this->userRepository->getUser($_COOKIE['user']);
        $this->render("upload-image", ['messages' => $this->messages, 'user' => $this->user]);
    }


    public function addMotivation(){
        if($this->isPost()
            && isset($_POST['set-motivation-button'])){
            $motivation = $_POST['motivation'];
            $motivationTextLen = strlen($motivation);
            if($motivationTextLen > 0){
                if(  $motivationTextLen> self::MAX_CHAR){
                    $this->messages[] = 'Unable to set motivation (text is too long). Please try again.';
                    $this->render("user-motivation", ['messages' => $this->messages,'user' => $this->user]);
                }
            }
            else{
                $this->messages[] = 'Unable to set motivation (text has no characters). Please try again.';
                $this->render("user-motivation", ['messages' => $this->messages,'user' => $this->user]);
            }
            $this->messages[] = $this->userRepository->updateMotivation($this->user, $motivation);
        }
        $this->render("user-motivation", ['messages' => $this->messages,'user' => $this->user]);
    }

    //TODO: test
    public function contact(){
        if($this->isPost() && isset($_POST['send-email-button'])){
            $to='gynkingcontact@gmail.com';
            $subject = 'GymKing';
            $message = $_POST['email-message'];
            $from = 'kacperskurski23@gmail.com';
            $headers = "From:" . $from;
            if(mail($to, $subject, $message,$headers)){
                $this->messages[] = 'Your mail has been sent successfully.';
            } else{
                $this->messages[] = 'Unable to send email. Please try again.';
            }
        }
        $this->render("contact", ['messages' => $this->messages,'user' => $this->user]);
    }

    private function validate(array $file) : bool{
        if($file['size'] > self::MAX_FILE_SIZE) {
            $this->messages[] = 'File is too large for destination file system.';
            return false;
        }
        if(!isset($file['type']) && !in_array($file['type'], self::SUPPORTED_TYPES)) {
            $this->messages[] = 'File type is not supported.';
            return false;
        }
        return true;
    }

}