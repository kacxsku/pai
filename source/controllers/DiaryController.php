<?php

require_once  __DIR__."/../repository/UserRepository.php";
require_once  __DIR__."/../repository/TrainingRepository.php";
require_once  __DIR__."/../repository/ExerciseRepository.php";
require_once  __DIR__."/../repository/TrainingDetailsRepository.php";


class DiaryController extends AppController
{

    private UserRepository $userRepository;
    private TrainingRepository $trainingRepository;
    private User $user;
    private TrainingDetailsRepository $trainingDetailsRepository;
    private ExerciseRepository $exerciseRepository;

    public function __construct(){
        parent::__construct();
        $this->userRepository = new UserRepository();
        $this->user = $this->userRepository->getUser($_COOKIE['user']);
        $this->trainingDetailsRepository = new TrainingDetailsRepository();
        $this->trainingRepository = new TrainingRepository();
        $this->exerciseRepository = new ExerciseRepository();
    }

    public function diary(){
        if($this->isPost() && isset($_POST['get-exercises-button'])) {
        $trainingDate = $_POST['calendar'];
        } else{
            $trainingDate = date('Y-m-d');
        }

        $training = $this->trainingRepository->getTraining($trainingDate);
        $trainingDetails = $this->trainingDetailsRepository->getTrainingDetails($training);
        $exercises = $this->exerciseRepository->getExercises($trainingDetails);

        $this->render('diary', ['user' => $this->user, 'exercises' => $exercises]);
    }

    public function add() {
        $contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';

        if($contentType === "application/json") {
            $content = trim(file_get_contents("php://input"));
            $decoded = json_decode($content, true);

            header('Content-type: application/json');
            http_response_code(200);

//            if($this->isPost() && isset($_POST['get-exercises-button'])) {
//                $trainingDate = $_POST['calendar'];
//            } else{
//                $trainingDate = date('Y-m-d');
//            }
//
//            $training = $this->trainingRepository->getTraining($trainingDate);
//            $trainingDetails = $this->trainingDetailsRepository->getTrainingDetails($training);
//            $exercises = $this->exerciseRepository->getExercises($trainingDetails);
//
//            echo json_encode($this->exerciseRepository->addExercise($decoded, $trainingDate));
        }

//        $this->render('diary', ['user' => $this->user, 'exercises' => $exercises]);
    }
}