<?php

require_once 'AppController.php';
require_once __DIR__.'/../models/User.php';
require_once __DIR__.'/../repository/UserRepository.php';
require_once __DIR__.'/../repository/NotificationRepository.php';
require_once __DIR__.'/../repository/TrainingRepository.php';
require_once __DIR__.'/../repository/TrainingDetailsRepository.php';
require_once __DIR__.'/../repository/ExerciseRepository.php';

class DefaultController extends AppController {

    private User $user;
    private UserRepository $userRepository;
    private NotificationRepository $notificationRepository;
    private TrainingRepository  $trainingRepository;
    private TrainingDetailsRepository  $trainingDetailsRepository;
    private ExerciseRepository $exerciseRepository;

    public function __construct(){
        parent::__construct();
        $this->userRepository = new UserRepository();
        $this->notificationRepository = new NotificationRepository();
        $this->user = $this->userRepository->getUser($_COOKIE['user']);
        $this->trainingRepository = new TrainingRepository();
        $this->trainingDetailsRepository = new TrainingDetailsRepository();
        $this->exerciseRepository = new ExerciseRepository();
    }

    public function index() {
        $this->render('login');
    }

    public function registration(){
       $this->render('registration');
    }

    public function faqs(){
       $this->render('FAQs',['user' => $this->user]);
    }

    public function account(){
        $notifications = $this->notificationRepository->getNotifications($this->user->getId());
        $this->render('account',['user' => $this->user,'notifications' => $notifications]);
    }

    public function schedule(){
        $actualDate = date("Y-m-d");
        $training = $this->trainingRepository->getTraining($actualDate);
        $trainingDetails = $this->trainingDetailsRepository->getTrainingDetails($training);
        $exercises = $this->exerciseRepository->getExercises($trainingDetails);

        $this->render('schedule', ['user' => $this->user,'exercises'=> $exercises, 'training' => $training]);
    }

    public function settings(){
        $this->render('settings', ['user' =>$this->user]);
    }

}