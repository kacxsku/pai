<?php

class User
{
    private  $id;
    private  $login;
    private  $password;
    private  $name;
    private  $userMotivation;
    private  $age;
    private  $userPhoto;
    private  $role;

    public function __construct($id,$login, $password, $name, $userMotivation ,$age,$userPhoto, $role){
        $this->id=$id;
        $this->login = $login;
        $this->password = $password;
        $this->name = $name;
        $this->age = $age;
        $this->role = $role;

        if($userPhoto === null || $userPhoto === ""){
            $this->userPhoto = 'placeholder.jpg';
        } else{
            $this->userPhoto = 'public/uploads/'.$userPhoto;
        }
        if($userMotivation === ""){
            $this->userMotivation = "Your motivation";
        } else {
            $this->userMotivation = $userMotivation;
        }
    }

    public function getId(){
        return $this->id;
    }


    public function setId($id): void{
        $this->id = $id;
    }

    public function getUserPhoto(): string{
        return $this->userPhoto;
    }

    public function setUserPhoto(string $user_photo): void{
        $this->userPhoto = $user_photo;
    }

    public function getPassword(){
        return $this->password;
    }

    public function setPassword($password): void{
        $this->password = $password;
    }

    public function getAge(){
        return $this->age;
    }

    public function setAge($age): void{
        $this->age = $age;
    }

    public function getLogin(){

        return $this->login;
    }

    public function setLogin($login): void{
        $this->login = $login;
    }

    public function getName(){
        return $this->name;
    }

    public function setName($name): void{
        $this->name = $name;
    }

    public function getUserMotivation(){
        return $this->userMotivation;
    }

    public function setUserMotivation($userMotivation): void{
        $this->userMotivation = $userMotivation;
    }

    public function getRole()
    {
        return $this->role;
    }

    public function setRole($role): void
    {
        $this->role = $role;
    }



}