<?php

class Result
{
    private  $id;
    private  $weight;
    //moze Exercise $exercise
    private  $exercise_name;

    public function __construct($id, $weight, $exercise_name){
        $this->id = $id;
        $this->weight = $weight;
        $this->exercise_name = $exercise_name;
    }

    public function getId(){
        return $this->id;
    }

    public function setId($id): void{
        $this->id = $id;
    }

    public function getWeight(){
        return $this->weight;
    }

    public function setWeight($weight): void{
        $this->weight = $weight;
    }

    public function getExerciseName(){
        return $this->exercise_name;
    }

    public function setExerciseName($exercise_name): void{
        $this->exercise_name = $exercise_name;
    }



}