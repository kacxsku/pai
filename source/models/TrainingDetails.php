<?php
require_once 'Training.php';

class TrainingDetails
{
    private  $id;
    private  $series;
    private  $repeats;
    private Training $training;


    public function __construct($id, $series, $repeats, Training $training){
        $this->id = $id;
        $this->series = $series;
        $this->repeats = $repeats;
        $this->training = $training;
    }

    public function getId(){
        return $this->id;
    }

    public function setId($id): void{
        $this->id = $id;
    }

    public function getSeries(){
        return $this->series;
    }

    public function setSeries($series): void{
        $this->series = $series;
    }

    public function getRepeats(){
        return $this->repeats;
    }

    public function setRepeats($repeats): void{
        $this->repeats = $repeats;
    }

    public function getTraining(): Training{
        return $this->training;
    }

    public function setTraining(Training $training): void{
        $this->training = $training;
    }
}