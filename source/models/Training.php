<?php

class Training
{
    private  $id;
    private  $date;
    private  $name;

    public function __construct($id, $date,  $name){
        $this->id = $id;
        $this->date = $date;
        $this->name = $name;
    }

    public function getId(){
        return $this->id;
    }

    public function setId($id): void{
        $this->id = $id;
    }

    public function getDate(){
        return $this->date;
    }

    public function setDate($date): void{
        $this->date = $date;
    }

    public function getName(){
        return $this->name;
    }

    public function setName($name): void{
        $this->name = $name;
    }
}