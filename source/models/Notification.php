<?php

class Notification
{

    private  $title;
    private  $body;

    public function __construct(string $title, string $body){
        $this->title = $title;
        $this->body = $body;
    }

    public function getTitle(){
        return $this->title;
    }

    public function setTitle($title): void{
        $this->title = $title;
    }

    public function getBody(){
        return $this->body;
    }

    public function setBody($body): void{
        $this->body = $body;
    }
}