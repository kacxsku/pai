<?php

require_once 'User.php';
require_once 'Trainer.php';

class Pupil
{
    private User $user;
    private Trainer $trainer;

    public function __construct(User $user, Trainer $trainer){
        $this->user = $user;
        $this->trainer = $trainer;
    }


    public function getTrainer(): Trainer{
        return $this->trainer;
    }

    public function setTrainer(Trainer $trainer): void{
        $this->trainer = $trainer;
    }

    public function getUser(): User{
        return $this->user;
    }

    public function setUser(User $user): void{
        $this->user = $user;
    }

    public function getUserName(): string{
        die($this->user->getName());
        return $this->user->getName();
    }
}