<?php

require_once 'TrainingDetails.php';

class Exercise
{
    private  $id;
    private  $name;
    private  $description;
    private TrainingDetails $trainingDetails;

    public function __construct($id,string $name,string $description, TrainingDetails $trainingDetails){
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
        $this->trainingDetails = $trainingDetails;
    }

    public function getId(){
        return $this->id;
    }

    public function setId($id): void{
        $this->id = $id;
    }

    public function getTrainingDetails(): TrainingDetails{
        return $this->trainingDetails;
    }

    public function setTrainingDetails(TrainingDetails $trainingDetails): void{
        $this->trainingDetails = $trainingDetails;
    }

    public function getName(){
        return $this->name;
    }

    public function setName($name){
        $this->name = $name;
    }

    public function getDescription(){
        return $this->description;
    }

    public function setDescription($description){
        $this->description = $description;
    }
}