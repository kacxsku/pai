<?php

class Trainer
{
    private  $id;
    private User $user;

    public function __construct($id, User $user){
        $this->id = $id;
        $this->user = $user;
    }

    public function getId(){
        return $this->id;
    }

    public function setId($id): void{
        $this->id = $id;
    }

    public function getUser(): User{
        return $this->user;
    }

    public function setUser(User $user): void{
        $this->user = $user;
    }

}