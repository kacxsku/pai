<?php

require_once __DIR__.'/../models/Notification.php';

class NotificationRepository extends Repository
{

    public function getNotifications(int $userId) : array
    {
        $result= [];

        $stmt = $this->database->connect()->prepare(
            '
                    SELECT notification.notification_id,notification.title, notification.body
                    FROM public.user_notification
                    INNER JOIN public."user" ON ("user".user_id = user_notification.id_user and "user".user_id=:id)
                    INNER JOIN  public.notification ON (notification.notification_id = user_notification.id_notification);
            '
        );

        $stmt->bindParam(':id', $userId, PDO::PARAM_STR);
        $stmt->execute();

        $notifications = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($notifications as $notification){
            $result[] = new Notification(
                $notification['title'],
                $notification['body']
            );
        }
        return $result;
    }
}