<?php
require_once __DIR__.'/../models/User.php';
require_once __DIR__.'/../models/Trainer.php';
require_once __DIR__.'/../models/Pupil.php';

class TrainerRepository extends Repository
{
    private UserRepository $userRepository;
    private User $actualUser;
    private int $idTrainer;

    public function __construct()
    {
        parent::__construct();
        $this->userRepository = new UserRepository();
        $this->actualUser = $this->userRepository->getUser($_COOKIE['user']);
        $this->idTrainer = $this->actualUser->getId();
    }

    public function addPupil(User $user): string {
        try{
            $stmt = $this->database->connect()->prepare(
                'INSERT INTO public.pupil(id_user, id_trainer)
                        VALUES (:idUser,:idTrainer);'
            );

            $idUser = $user->getId();

            $stmt->bindParam(':idUser', $idUser, PDO::PARAM_INT);
            $stmt->bindParam(':idTrainer', $this->idTrainer, PDO::PARAM_INT);
            $stmt->execute();

            return "user added";
        } catch (PDOException $e){
            return $e->getMessage();
        }
    }

    public function getAllPupils() : array{
        $result = [];
        $stmt = $this->database->connect()->prepare(
            '
                    SELECT u.email
                    FROM public.user u
                    INNER JOIN public.pupil p ON p.id_user=u.user_id
                    INNER JOIN public.trainer t ON p.id_trainer = t.trainer_id
                    WHERE t.trainer_id = :idTrainer;
                    '
        );

        $trainer = $this->getTrainer($this->actualUser);
        $id = $trainer->getId();
        $stmt->bindParam(':idTrainer', $id, PDO::PARAM_INT);
        $stmt->execute();

        $pupils = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($pupils as $pupil){
            $result[] = new Pupil(
                $this->userRepository->getUser($pupil['email']),
                $trainer
            );
        }
        return $result;
    }


    public function getTrainer(User $user): ?Trainer{
        $stmt = $this->database->connect()->prepare(
            '
                SELECT trainer_id
                FROM public."trainer" WHERE id_user = :idUser;
                '
        );

        $idUser = $user->getId();
        $stmt->bindParam(':idUser', $idUser, PDO::PARAM_STR);
        $stmt->execute();

        $trainer = $stmt->fetch(PDO::FETCH_ASSOC);

        if($trainer == false) {
            return null;
        }

        return new Trainer(
            $trainer['trainer_id'],
            $user
        );
    }


    public function getPupilByName(string $searchString) :array {

        $searchString = '%'.strtolower($searchString).'%';

        $stmt = $this->database->connect()->prepare(
          '
                    SELECT u.name
                    FROM public.user u
                    INNER JOIN public.pupil p ON p.id_user=u.user_id
                    INNER JOIN public.trainer t ON p.id_trainer = t.trainer_id
                    WHERE t.trainer_id = :idTrainer and LOWER(u.name) LIKE :searchString; 
                 '
        );

        $trainer = $this->getTrainer($this->actualUser);
        $idTrainer = $trainer->getId();
        $stmt->bindParam(':idTrainer', $idTrainer, PDO::PARAM_INT);

        $stmt->bindParam(':searchString', $searchString, PDO::PARAM_STR);

        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
}