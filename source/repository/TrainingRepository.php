<?php
require_once __DIR__.'/../models/Training.php';
require_once __DIR__.'/../models/Exercise.php';

class TrainingRepository extends Repository
{

    public function getTrainingName(): string{
        $stmt = $this->database->connect()->prepare(
            '
                    SELECT t.name FROM public.training t 
                    WHERE t.date = TO_DATE(:date, \'YYYY-MM-DD\');
                  '
        );

        $stmt->bindParam(':date', $date, PDO::PARAM_STR);
        $stmt->execute();

        $training = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($training == false) {
            return "";
        }

        return $training['name'];
    }

//        public function getTraining(string $date): array
//    {
//        $result = [];
//        //rozdzielic to na mniejsze queries?
//        $stmt = $this->database->connect()->prepare(
//            '
//            SELECT t.training_id ,t.date, t.name,
//                   td.training_details_id ,td.series, td.repeats, td.id_training,
//                   e.exercise_id ,e.exercise_name, e.description, e.id_training_details
//            FROM public.training t
//            INNER JOIN training_details td on t.training_id = td.id_training
//            INNER JOIN exercise e on td.training_details_id = e.id_training_details
//            WHERE t.date = TO_DATE(:date, \'YYYY-MM-DD\')
//            '
//        );
//
//        $stmt->bindParam(
//            ':date',
//            $date,
//            PDO::PARAM_STR);
//
//        $stmt->execute();
//
//        $trainingData = $stmt->fetch(PDO::FETCH_ASSOC);
//
//        $training = new Training(
//            $trainingData['training_id'],
//            $trainingData['date'],
//            $trainingData['name']
//        );
//
//        $trainingDetailsData = new TrainingDetails(
//            $trainingData['training_details_id'],
//            $trainingData['series'],
//            $trainingData['repeats'],
//            $training
//        );
//
//        foreach ($trainingData as $training){
//            $result[] = new Exercise(
//                $trainingData['exercise_id'],
//                $trainingData['exercise_name'],
//                $trainingData['description'],
//                $trainingDetailsData
//            );
//        }
//
//        return $result;
//    }

    public function getTraining(string $date): ?Training {
        $stmt = $this->database->connect()->prepare(
            '
                    SELECT t.training_id ,t.date, t.name
                    FROM public.training t
                    WHERE t.date = TO_DATE(:date, \'YYYY-MM-DD\')
                    '
        );

        $stmt->bindParam(':date', $date, PDO::PARAM_STR);
        $stmt->execute();

        $training = $stmt->fetch(PDO::FETCH_ASSOC);

        return new Training(
            $training['training_id'],
            $training['date'],
            $training['name']
        );
    }

    public function getAllUsersPlan(){

    }


}