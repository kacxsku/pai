<?php
require_once __DIR__.'/../models/Exercise.php';
require_once __DIR__.'/../models/Result.php';

class ExerciseRepository extends Repository
{
    private UserRepository  $userRepository;

    public function __construct(){
        parent::__construct();
        $this->userRepository =new UserRepository();
    }

    public function getExercises(TrainingDetails $trainingDetails) : array {
        $result = [];

        $stmt = $this->database->connect()->prepare(
            '
            SELECT e.exercise_id, e.exercise_name, e.description
            FROM public.exercise e
            WHERE e.id_training_details = :detailsId
            '
        );

        $detailsId = $trainingDetails->getId();
        $stmt->bindParam(':detailsId', $detailsId, PDO::PARAM_STR);
        $stmt->execute();
        $exercises = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($exercises as $exercise){
            $result [] = new Exercise(
                $exercise['exercise_id'],
                $exercise['exercise_name'],
                $exercise['description'],
                $trainingDetails
            );
        }
        return $result;
    }

    public function getAllUsersExercises($userId): array {
        $result = [];
        $stmt = $this->database->connect()->prepare(
            '
                    SELECT e.exercise_name
                    FROM public.exercise e
                    INNER JOIN training_details td ON td.training_details_id = e.id_training_details
                    INNER JOIN training t on t.training_id = td.id_training
                    INNER JOIN pupil p on p.pupil_id = t.id_pupil
                    INNER JOIN "user" u on u.user_id = p.id_user
                    WHERE u.user_id = :userId ;
            '
        );

        $stmt->bindParam(':userId', $userId, PDO::PARAM_INT);
        $stmt->execute();

        $exercises = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($exercises as $exercise){
            $result [] = $exercise['exercise_name'];
        }
        return $result;
    }


    public function addExercise(array $exerciseDetails, string $trainningDate) :array {

        $exerciseName = $exerciseDetails["name"];
        $exerciseWeight = $exerciseDetails["weight"];
        $exerciseSeries = $exerciseDetails["series"];
        $exerciseReps = $exerciseDetails["reps"];


        $stmt = $this->database->connect()->prepare(
                'INSERT INTO public.pupil(id_user, id_trainer)
                        VALUES (:idUser,:idTrainer);'
        );

        $stmt->bindParam(':name',$exerciseName,PDO::PARAM_STR);
        $stmt->bindParam(':weight',$exerciseWeight,PDO::PARAM_STR);
        $stmt->bindParam(':series',$exerciseSeries,PDO::PARAM_INT);
        $stmt->bindParam(':reps',$exerciseReps,PDO::PARAM_INT);

        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);

    }


    public function getResults($exercise, $userId): array{
        $result = [];

        $stmt = $this->database->connect()->prepare(
            '
                    SELECT r.id_result, r.weight, e.exercise_name
                    FROM public.result r
                    INNER JOIN exercise e on r.id_exercise=e.exercise_id
                    INNER JOIN training_details td ON td.training_details_id = e.id_training_details
                    INNER JOIN training t on t.training_id = td.id_training
                    INNER JOIN pupil p on p.pupil_id = t.id_pupil
                    INNER JOIN "user" u on u.user_id = p.id_user
                    WHERE u.user_id = :userId;
            '
        );

        $trainerId = $this->userRepository->getUser($_COOKIE['user'])->getId();
        $stmt->bindParam(':userId', $trainerId, PDO::PARAM_STR);

        $stmt->execute();

        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($results as $result){
            $result [] = new Result(
                $result['id_result'],
                $result['weight'],
                $result['exercise_name']
            );
        }
        return $result;
    }


    public function getResultsByExerciseName($exerciseName): array
    {
        $stmt = $this->database->connect()->prepare(
            '
                    SELECT r.weight, t.date FROM public.result r
                    INNER JOIN exercise e on e.exercise_id = r.id_exercise
                    INNER JOIN training_details td on td.training_details_id = e.id_training_details
                    INNER JOIN training t on t.training_id = td.id_training
                    WHERE e.exercise_name = :exerciseName;
            '
        );

        $stmt->bindParam(':exerciseName', $exerciseName, PDO::PARAM_STR);

        $stmt->execute();

        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $resultTable = [];

        foreach ($results as $result){
            $resultTable [] = new Result(
                $result['id_result'],
                $result['weight'],
                $result['exercise_name']
            );
        }
        return $resultTable;
    }
}