<?php

require_once 'Repository.php';
require_once __DIR__.'/../models/User.php';

class UserRepository extends Repository
{
    public function getUser( $email) :?User {
        $stmt = $this->database->connect()->prepare(
        '
                SELECT u.user_id, 
                       u.email, 
                       u.password_hash, 
                       u.name, 
                       u.user_motivation, 
                       u.age, 
                       u.image, 
                       r.role_name
                FROM public."user" u
                INNER JOIN user_role ur on  ur.id_user = u.user_id 
                INNER JOIN role r on r.role_id = ur.id_role 
                WHERE email = :email;
                '

        );

        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
        $stmt->execute();

        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if($user == false) {
            return null;
        }

        return new User(
            $user['user_id'],
            $user['email'],
            $user['password_hash'],
            $user['name'],
            $user['user_motivation'],
            $user['age'],
            $user['image'],
            strtoupper($user['role_name'])
        );
    }

    public function saveUser(User $user): String{
        $email =$user->getLogin();
        if($this->getUser($email)!=null){
            return "User already exist";
        }

        try{
            $stmt = $this->database->connect()->prepare(
                '
                        INSERT INTO public.user(email,password_hash,name,age,image,user_motivation) 
                        VALUES (:email,:passwordHash,:name,:age,:image,:user_motivation);
                        '
            );

            $password = $user->getPassword();
            $name = $user->getName();
            $age = $user->getAge();
            $image = $user->getUserPhoto();
            $user_motivation = $user->getUserMotivation();


            $stmt->bindParam(':passwordHash', $password, PDO::PARAM_STR);
            $stmt->bindParam(':email', $email, PDO::PARAM_STR);
            $stmt->bindParam(':name', $name, PDO::PARAM_STR);
            $stmt->bindParam(':age', $age, PDO::PARAM_STR);
            $stmt->bindParam(':image', $image, PDO::PARAM_STR);
            $stmt->bindParam(':user_motivation', $user_motivation, PDO::PARAM_STR);

            $stmt->execute();
            return "User created";

        } catch (PDOException $e){
            die($e->getMessage());
            return "invalid data";
        }
    }


    public function updatePhoto($userName, $userPhotoUrl): string {
        try{
            $stmt = $this->database->connect()->prepare(
                '
                        UPDATE public."user" SET image=:photoUrl WHERE email = :userName;
                        '
            );

            $stmt->bindParam(':photoUrl', $userPhotoUrl, PDO::PARAM_STR);
            $stmt->bindParam(':userName', $userName, PDO::PARAM_STR);
            $stmt->execute();

            return "Photo updated";
        } catch (PDOException $e){
            return "error";
        }
    }


    public function updateMotivation($user, $user_motivation): string{
        try{
            $stmt = $this->database->connect()->prepare(
                '
                        UPDATE "user" 
                        SET user_motivation= :motivation 
                        WHERE email = :email;
                        '
            );

            $stmt->bindParam(':motivation', $user_motivation, PDO::PARAM_STR);
            $stmt->bindParam(':email', $user->getLogin(), PDO::PARAM_STR);
            $stmt->execute();

            return "Motivation updated";
        } catch (PDOException $e){
            return "error";
        }
    }

}