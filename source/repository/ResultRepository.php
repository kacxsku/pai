<?php
require_once __DIR__.'/../models/Person.php';

class ResultRepository extends Repository
{

    public function getWeightDatePairs(string $exerciseName, string $userName) : array{

        $result= [];

        $stmt = $this->database->connect()->prepare(
            '
            
            '
        );

        $stmt->bindParam(':exerciseName', $userName, PDO::PARAM_STR);
        $stmt->bindParam(':exerciseName', $userName, PDO::PARAM_STR);
        $stmt->execute();

        $exerciseMaxResults = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($exerciseMaxResults as $maxResult){
            $result[] = array(
                $maxResult['title'],
                $maxResult['body']);
        }
        return $result;
    }

}