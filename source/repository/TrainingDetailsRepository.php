<?php
require_once 'Repository.php';

class TrainingDetailsRepository extends Repository
{

    public function getTrainingDetails(Training $training): TrainingDetails {

        $stmt = $this->database->connect()->prepare(
            '
                    SELECT td.training_details_id, td.series,td.repeats
                    FROM public.training_details td
                    WHERE td.id_training = :trainingId
                    '
        );

        $trainingId = $training->getId();
        $stmt->bindParam(':trainingId', $trainingId, PDO::PARAM_STR);
        $stmt->execute();

        $trainingDetails = $stmt->fetch(PDO::FETCH_ASSOC);

        return new TrainingDetails(
            $trainingDetails['training_details_id'],
            $trainingDetails['series'],
            $trainingDetails['repeats'],
            $training
        );
    }

}