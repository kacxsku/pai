<?php

require 'Routing.php';

$path = trim($_SERVER['REQUEST_URI'], '/');
$path = parse_url($path, PHP_URL_PATH);

Routing::get('','DefaultController');
Routing::get('account','DefaultController');
Routing::get('diary','DiaryController');
Routing::get('schedule','DefaultController');
Routing::get('settings','DefaultController');
Routing::get('faqs','DefaultController');
Routing::get('stats','StatsController');
Routing::get('contact','UserController');
Routing::get('addMotivation', 'UserController');
Routing::get('myPupils','TrainerController');
Routing::get('myPlans','TrainerController');
Routing::get('addPlan','TrainerController');
Routing::get('addPupil','TrainerController');


Routing::post('login','SecurityController');
Routing::post('logout','SecurityController');
Routing::post('uploadImage', 'UserController');
Routing::post('registration', 'SecurityController');
Routing::post('add','DiaryController');
Routing::post('search', 'TrainerController');


Routing::run($path);

