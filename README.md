# GymKing
![](screenshots/img.png)

* [General Info](#general-info "Goto General Info")
* [Description](#description "Goto General Description")
* [Kryteria Ewaluacji projektu](#description "Goto General Kryteria ewaulacji projektu")
* [Technologies](#technologies "Goto General Technologies")
* [Screenshots](#Screenshots "Goto General Screenshots")

## General Info
Zadaniem aplikacji GymKing jest ulatwienie planowania treningów oraz łatwiejszy przegląd w progres siłowy,
Przez podział na role użytkownik i trener aplikacja może być przydatna dla osób korzystających z treningów personalnych 
nie na każdym treningu a tylko w poszczególne dni przez co trener może sprawdzać progres podopiecznego
Dzięki responsywności z aplikacji mozna korzystac nawet na siłowni dzięki czemu użytkownik nie zapomni wykonać jakiegoś 
ćwiczenia.
## Description
Widok strony głownej zawiera przycisk rejestracji, formularz logowania oraz pola z informacjami o stronie
Widoki kolejnych stron zachowane są jako one page by nawigacja po stronie przebiagała łatwiej i w jednym miejscu mieć skondensowane dane
* Cala strona ma ustalone parametry jako vh i vw co pozwala na jej skalowanie, mimo to dla urządzeń mobilnych z  o wymiarach mniejszych niz 414px szerokości strona jest responsywna.

* po wejsciu na strone użytkownik ma możliwość zalogowania się bądź rejestracji, wszystkie wprowadzone przez użytkownika dane są sprawdzane pod kątem poprawności

* Po zalogowaniu wyświetla się strona konta użytkownika w której w zależności od nadanej roli wyświetlają się różne elementy:
    * dla roli trenera są to przyciski pozwalające na zarządzanie podopiecznymi
    * dla roli podopiecznego jest to pole motivation gdzie użytkownik może wpisać co motywuje go do treningów

* W zakładce schedule wyświetlana jest lista ćwiczeń na dany dzien

* W zakładce diary użytkownik ma możliwość dodawania swoich ćwiczeń i oznaczania ich jako wykonane

* W zakładce stats użytkownik ma możliwość sprawdzania swojego progresu przez generowanie wykresów dla każdego ćwiczenia

* W zakładce settings użytkownik ma możliwość zmiany zdjęcia profilowego, włączenia trybu ciemnego, wysłania e-mail do supportu strony, sprawdzenia FAQs, i pobrania linku do strony w celu zaproszenia innych

## Kryteria Ewaluacji projektu
* [x] 1.DOKUMENTACJE W README.MD
* [x] 2.KOD NAPISANY OBIEKTOWO (CZĘŚĆ BACKENDOWA)
* [x] 3.DIAGRAM ERD
* [x] 4.GIT
* [ ] 5.REALIZACJA TEMATU
* [x] 6.HTML
* [x] 7.POSTGRESQL
* [x] 8.ZŁOŻONOŚĆ BAZY DANYCH
* [x] 9.PHP
* [x] 10.JAVA SCRIPT
* [x] 11.FETCH API (AJAX)
* [x] 12.DESIGN
* [x] 13.RESPONSYWNOŚĆ
* [x] 14.LOGOWANIE
* [x] 15.SESJA UŻYTKOWNIKA
* [x] 16.UPRAWNIENIA UŻYTKOWNIKÓW
* [x] 17.ROLE UŻYTKOWNIKÓW
* [x] 18.WYLOGOWYWANIE
* [x] 19.WIDOKI, WYZWALACZ/ FUNKCJE, TRANSAKCJE NA BAZIE DANYCH
* [x] 20.AKCJE NA REFERENCJACH
* [x] 21.BEZPIECZEŃSTWO
* [x] 22.BRAK REPLIKACJI KODU
* [x] 23.CZYSTOŚĆ I PRZEJRZYSTOŚĆ KODU
* [x] 24.BAZA DANYCH ZRZUCONA DO PLIKU .SQL*

## Diagram ERD
##### diagram wygenerowany przez program PhpStorm
![](erdStorm.png)
##### diagram wykonany przezemnie
![](erd.png)

## Technologies
* Html
* Css
* JavaScript
* PostgreSQL
* PHP version 7.4.3-fmp-alpine3.11
* Nginx version 1.17.8-alpine

## Screenshots
* PC (1920x1080)
  * wspolne widoki
  
![](screenshots/PC/img_8.png)
![](screenshots/PC/img.png)
![](screenshots/PC/img_1.png)
![](screenshots/PC/img_7.png)
![](screenshots/PC/img_9.png)
  * trener

![](screenshots/PC/img_2.png)
![](screenshots/PC/img_3.png)

  * podopieczny

![](screenshots/PC/img_4.png)
![](screenshots/PC/img_5.png)
    
* Responsywne (Iphone SE 375x667)
  * wspolne widoki
  
![](screenshots/Responsive/img.png)
![](screenshots/Responsive/img_1.png)
![](screenshots/Responsive/img_2.png)
![](screenshots/Responsive/img_3.png)
![](screenshots/Responsive/img_4.png)

  * trener

![](screenshots/Responsive/img_5.png)
![](screenshots/Responsive/img_8.png)

  * podopieczny

![](screenshots/Responsive/img_6.png)
![](screenshots/Responsive/img_7.png)